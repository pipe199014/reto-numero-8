package com.choucair.formacion.steps;

import java.util.List;

import org.fluentlenium.core.annotation.Page;

import com.choucair.formacion.pageobjects.CRMZohoCrearTareaPage;

public class CRMZohoCrearTareaStep {
	
	@Page
	CRMZohoCrearTareaPage cRMZohoCrearTareaPage;
	
	public void ingresoUsuarioRegistrado(String usuario, String contra) throws InterruptedException
	{
		cRMZohoCrearTareaPage.open();
		cRMZohoCrearTareaPage.ingreso(usuario, contra);
	}
	
	public void crearTarea(List<String> data) throws InterruptedException
	{
		cRMZohoCrearTareaPage.crearTarea(data);
	}

	
	public void verificoTareaCreada(List<String> data) throws InterruptedException
	{
		cRMZohoCrearTareaPage.verificoTareaCreada(data);
	}
}

package com.choucair.formacion.steps;

import org.fluentlenium.core.annotation.Page;

import com.choucair.formacion.pageobjects.CRMZohoPage;

import net.thucydides.core.annotations.Step;

public class CRMZohoSteps {
	
	@Page
	CRMZohoPage cRMZohoPage;
	
	@Step
	public void ingresarPaginaCRMZoho() throws InterruptedException
	{
		cRMZohoPage.open();
		Thread.sleep(2000);
	}
	
	@Step
	public void realizarRegistro() throws InterruptedException
	{
		cRMZohoPage.realizarRegistro();
	}
	
	@Step
	public void verificarAcceso()
	{
		cRMZohoPage.verificarAcceso();
	}
	

}

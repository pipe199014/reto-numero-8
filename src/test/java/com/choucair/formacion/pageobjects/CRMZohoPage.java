package com.choucair.formacion.pageobjects;

import java.util.List;

import org.hamcrest.MatcherAssert;
import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://www.zoho.com/es-xl/crm/")
public class CRMZohoPage extends PageObject {

	@FindBy(name = "firstname")
	private WebElementFacade txtNombre;

	@FindBy(id = "email")
	private WebElementFacade txtCorreo;

	@FindBy(name = "password")
	private WebElementFacade txtContra;

	@FindBy(className = "za-tos-container")
	private WebElementFacade chkTerminos;

	@FindBy(id = "signupbtn")
	private WebElementFacade btnComenzar;

	@FindBy(id = "email-error")
	private WebElementFacade msjRegistro;

	@FindBy(xpath = "//*[contains(@href, '/accounts.zoho.com/signin?')]")
	private WebElementFacade lnkInicioSesion;

	@FindBy(id = "lid")
	private WebElementFacade txtCorreoInicio;

	@FindBy(id = "pwd")
	private WebElementFacade txtContraseniaInicio;

	@FindBy(id = "signin_submit")
	private WebElementFacade btnIniciarSesion;

	@FindBy(id = "show-uName")
	private WebElementFacade msjBienvenida;

	public void realizarRegistro() throws InterruptedException {

		txtNombre.sendKeys("Felipe");
		txtCorreo.sendKeys("fgomezv@choucairtesting.com");
		txtContra.sendKeys("123456789");
		Thread.sleep(2000);

		List<WebElement> lchecks = chkTerminos.findElements(By.tagName("input"));
		for (int i = 0; i < lchecks.size(); i++) {
			if (lchecks.get(i).getAttribute("outerHTML").contains("checked")) {
				System.out.println(lchecks.get(i).getText() + "Esta seleccionado");
			} else {
				lchecks.get(i).click();
				System.out.println(lchecks.get(i).getAttribute("outerHTML") + "Toco seleccionar");
				break;
			}
		}

		btnComenzar.click();

		for (int i = 0; i < 50; i++) {
			if (msjRegistro.isCurrentlyVisible())
				lnkInicioSesion.click();
			Thread.sleep(6000);
			txtCorreoInicio.sendKeys("fgomezv@choucairtesting.com");
			txtContraseniaInicio.sendKeys("123456789");
			btnIniciarSesion.click();
			Thread.sleep(8000);
			break;
		}

	}

	public void verificarAcceso() {
		MatcherAssert.assertThat("No ingresó", msjBienvenida.isDisplayed());
	}

}

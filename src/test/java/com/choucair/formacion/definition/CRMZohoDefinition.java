package com.choucair.formacion.definition;

import com.choucair.formacion.steps.CRMZohoSteps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class CRMZohoDefinition {
	
	@Steps
	CRMZohoSteps rRMZohoSteps;
	
	@Given("^que el usuario quiere utilizar el CRM Zoho$")
	public void ingresarPaginaCRMZoho() throws Throwable {
		rRMZohoSteps.ingresarPaginaCRMZoho();
	}

	@When("^realizo el registro exitoso$")
	public void realizarRegistro() throws Throwable {
		rRMZohoSteps.realizarRegistro();
	}

	@Then("^verifico el acceso a la aplicación$")
	public void verificarAcceso() throws Throwable {
		rRMZohoSteps.verificarAcceso();
	}

}

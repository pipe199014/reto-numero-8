package com.choucair.formacion.definition;

import java.util.List;

import com.choucair.formacion.steps.CRMZohoCrearTareaStep;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class CRMZohoCrearTareaDefinition {

	@Steps
	CRMZohoCrearTareaStep cRMZohoCrearTareaStep;
	
	List<String> lDatosTarea;
	
	@Given("^que ingreso al CRM Zoho con un usuario registrado \"([^\"]*)\" y \"([^\"]*)\"$")
	public void ingresoUsuarioRegistrado(String usuario, String contra) throws Throwable {
		cRMZohoCrearTareaStep.ingresoUsuarioRegistrado(usuario, contra);
	}

	@When("^realizo la creación de una Tarea$")
	public void creacionTarea(DataTable dtDatosFormulario) throws Throwable {
		
		List<List<String>> lDatos = dtDatosFormulario.raw();

		for (int i = 1; i < lDatos.size(); i++) {
			lDatosTarea = lDatos.get(i);
		}
		
		cRMZohoCrearTareaStep.crearTarea(lDatosTarea);
	}

	@Then("^verifico tarea creada exitosamente$")
	public void verificoTareaCreada() throws Throwable {
		cRMZohoCrearTareaStep.verificoTareaCreada(lDatosTarea);
	}
}

#Author: fgomezv.bancolombia.com.co

@Regresion
Feature: Trabajar con el CRM Zoho

  @Registro
  Scenario: Registrarme en la app CRM Zoho
    Given  que el usuario quiere utilizar el CRM Zoho
    When realizo el registro exitoso
    Then verifico el acceso a la aplicación
    
  @CrearTarea  
Scenario: Creación de Tarea
Given que ingreso al CRM Zoho con un usuario registrado "fgomezv@choucairtesting.com" y "123456789"
When realizo la creación de una Tarea
|Asunto_0 | Fecha de vencimiento_1|Contacto_2    | Cuenta_3 | Estado_4 | Prioridad_5 | Enviar msjemail_6 | Fecha inicio_7     | Tipo repetición_8 | Notificar_9      |Descipción_10|								
|Reunión  | 05/08/2018            | Kris Marrier | King     | En curso | Más alto    | Si |                01/05/2018 - 08:00 | Semanal           | Ventana emergente| Tarea creada|	
Then verifico tarea creada exitosamente
    


